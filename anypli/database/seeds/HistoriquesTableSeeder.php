<?php

use Illuminate\Database\Seeder;

use App\Historique;

class HistoriquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // Let's truncate our existing records to start from scratch.
        Historique::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Historique::create([
            	'appareil_id' => $faker->numberBetween(1,200),
                'employe_id' => $faker->numberBetween(1,200),
                'debut' => $faker->date($format = 'Y-m-d'),
                'fin' => $faker->date($format = 'Y-m-d'),

            ]);
        }

    }
}
