<?php

use Illuminate\Database\Seeder;

use App\Employe;

class EmployesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // Let's truncate our existing records to start from scratch.
        Employe::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Employe::create([
                'nom' => $faker->name,
                'prenom' => $faker->name,
                'specialite' => $faker->word,
                'email' => $faker->email,
                'tel' => $faker->numberBetween(100000,100000000),

            ]);
        }

    }
}
