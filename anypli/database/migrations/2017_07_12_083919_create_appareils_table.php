<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppareilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appareils', function (Blueprint $table) {
            $table->increments('id');
            $table->string('categorie');
            $table->string('marque');
            $table->string('reference');
            $table->string('microprocesseur');
            $table->Integer('cache');
            $table->Integer('ram');
            $table->Integer('disque_dur');
            $table->string('os');
            $table->string('qrcode');
            $table->boolean('disponible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appareils');
    }
}
