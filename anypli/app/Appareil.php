<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appareil extends Model
{
    protected $fillable= ['categorie', 'marque', 'reference', 'microprocesseur', 'cache', 'ram', 'disque_dur', 'os', 'qrcode', 'disponible'];


     /**
     * Get the 'historiques' for the 'appareil'.
     */
     
    public function historiques()
    {
        return $this->hasMany('App\Historique');
    }
}
