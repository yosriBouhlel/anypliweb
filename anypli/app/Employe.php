<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
   protected $fillable = ['nom', 'prenom', 'specialite', 'email', 'tel'];


     /**
     * Get the 'historiques' for the 'employe'.
     */
     
    public function historiques()
    {
        return $this->hasMany('App\Historique');
    }


}
