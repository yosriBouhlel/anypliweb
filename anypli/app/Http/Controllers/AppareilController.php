<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

use App\Appareil;
use App\Historique;
use App\Employe;
use DB;
use Intervention\Image\Facades\Image;

class AppareilController extends Controller
{
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $appareils = Appareil::paginate(15);

            return view('appareils.home', compact('appareils'));
        }


        /**
         * Display the specified resource.
         *
         * @param  App\Appareil $appareil
         * @return \Illuminate\Http\Response
         */
        public function show(Appareil $appareil)
        {
         return view('appareils.details', compact('appareil'));
     }



        /**
         * Remove the specified resource from storage.
         *
         * @param  App\Appareil $appareil
         * @return \Illuminate\Http\Response
         */
        public function delete(Appareil $appareil)
        {

            $historiques = Historique::where('appareil_id', '=', $appareil->id)
                                        ->get();

            foreach ($historiques as $historique) 
            {
                $historique->delete();
                                            
            }                            
            $appareil->delete();
            return redirect('/appareils');
        }


        /**
         * Search for the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function search(Request $request)
        {
            $categorie = $request->input('categorie');
            $marques = $request->input('marques');

            if($request->has('marques')){

                $appareils =  Appareil::where([
                    ['categorie', '=', $categorie], 
                    ['marque', 'LIKE', '%'.$marques.'%'],
                    ])
                ->orderBy('created_at', 'desc')
                ->paginate(15);

            }else{

               $appareils =  Appareil::where('categorie', '=', $categorie)
               ->orderBy('created_at', 'desc')
               ->paginate(15);           
           }

           return view('appareils.home', compact('appareils'));
       }



        /**
         * Search for the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function searchHistorique(Request $request)
        {
            $debut  = $request->input('datetimepickerdebut');
            $fin    = $request->input('datetimepickerfin');

            $id = $request->input('appareil_id');

            if($request->has('datetimepickerdebut') && $request->has('datetimepickerfin')){

                $historiques =  Historique::where([
                    ['debut', '>=', $debut], 
                    ['fin', '<', $fin],
                    ['appareil_id', '=', $id],
                    ])
                ->get();

            }elseif ($request->has('datetimepickerdebut') && !$request->has('datetimepickerfin')) {
                
                $historiques =  Historique::where([
                    ['debut', '>=', $debut], 
                    ['appareil_id', '=', $id],
                    ])
                ->get();

            }elseif (!$request->has('datetimepickerdebut') && $request->has('datetimepickerfin')) {
                

                $historiques =  Historique::where([
                    ['fin', '<', $fin], 
                    ['appareil_id', '=', $id],
                    ])
                ->get();
            }else{

                $historiques =  Historique::where([
                    ['appareil_id', '=', $id],
                    ])
                ->get();         
            }

            return view('appareils.historique', compact('historiques'));
        }    




        /**
         * Get the 'historique' for an 'appareil' .
         *
         * @param  App\Appareil $appareil
         * @return \Illuminate\Http\Response
         */
        public function getHistorique(Appareil $appareil)
        {
            $historiques = Historique::where('appareil_id', '=', $appareil->id)->get();
            
            return view('appareils.historique', compact('historiques'));
        }


        /**
         * Download the QR code for a specific 'appareil'.
         *
         * @param  App\Appareil $appareil
         * @return \Illuminate\Http\Response
         */
        public function download(Appareil $appareil)
        {

            //the file is stored under project/public/download/filename
            $pathToFile = public_path(). $appareil->qrcode;
            $name = $appareil->categorie .'_'. $appareil->marque .'_'. $appareil->reference;

            return response()->download($pathToFile, $name);

        }


        /************************************ Api methods ************************************/






        /**
         * Get the list of 'appareils'
         *
         * @return \Illuminate\Http\Response
         */
        public function indexApi()
        {
            $appareils = Appareil::all();

            return response()->json(['result' => $appareils], Response::HTTP_OK);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function storeApi(Request $request)
        {
            $chemin = $request->input("qrcode");

            $appareil = Appareil::create($request->all());

            DB::table('appareils')
            ->where('id', $appareil->id)
            ->update(['qrcode' => $chemin . $appareil->id . ".png"]);

            return response()->json(['id' => $appareil->id], Response::HTTP_OK);
        }




        /**
         * Upload a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function uploadApi(Request $request)
        {

           if( $request->hasFile("file") ) {
            $file = $request->file("file");
            $request->file("file")->move(public_path(). "/downloads/", $request->input("filename"));
        }
        return response()->json(Response::HTTP_OK);
    }



        /**
         * Display the specified resource.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function showApi(Request $request)
        {
            $appareil = Appareil::find($request->input('appareil_id'));

            $historique = Historique::where('appareil_id', '=', $request->input("appareil_id"))
            ->orderBy('created_at', 'desc')
            ->first();

            if ($historique != null) {
                
                $employe = Employe::find($historique->employe_id);
                $name = $employe->nom . " " . $employe->prenom;

            }else
            
            $name = "";

            if ($appareil != null) {
                return response()->json(['result' => $appareil, 'name' => $name], Response::HTTP_OK);
            }else
            return response()->json($appareil, Response::HTTP_NO_CONTENT);
            
        }



        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function updateApi(Request $request)
        {

            Historique::where([
                ['appareil_id', '=', $request->input("id")], 
                ['fin', '=', null],
                ])
            ->update(['fin' => date('Y-m-d')]);


            Appareil::where('id', $request->input("id"))
            ->update(['disponible' => 1]);

            return response()->json(Response::HTTP_OK);
        }

        

        /**
         * Remove the specified resource from storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function deleteApi(Request $request)
        {

            $historiques = Historique::where('appareil_id', '=', $request->input('id'))
                                        ->get();

            foreach ($historiques as $historique) 
            {
                $historique->delete();
                                            
            }

            $appareil = Appareil::find($request->input('id'));
            
            \File::delete(public_path().$appareil->qrcode);

            Appareil::destroy($request->input('id'));

            return response()->json(Response::HTTP_OK);
        }



        /**
         * Get the 'historique' for an 'appareil' .
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function getHistoriqueApi(Request $request)
        {
            $historiques = Historique::where('appareil_id', '=', $request->input('appareil_id'))
            ->get();

            $historiquesList = array();

            foreach ($historiques as $historique) {

                $h = array();
                
                $employe = Employe::find($historique->employe_id);

                $h["historique_id"] = $historique->id;
                $h["employe_id"]    = $historique->employe_id;
                $h["name"]          = $employe->nom . " " . $employe->prenom;
                $h["debut"]         = $historique->debut;
                $h["fin"]           = $historique->fin;

                array_push($historiquesList, $h);
                unset($h);
            }
            
            return response()->json(['result' => $historiquesList], Response::HTTP_OK);
        }




        /**
         * Search for the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function searchHistoriqueApi(Request $request)
        {
            $debut = $request->input('datetimepickerdebut');
            $fin = $request->input('datetimepickerfin');
            
            $id = $request->input('appareil_id');

            if($request->has('datetimepickerdebut') && $request->has('datetimepickerfin')){

                $historiques = Historique::where([
                    ['debut', '>=', $debut], 
                    ['fin', '<', $fin],
                    ['appareil_id', '=', $id],
                    ])
                ->get();

            }elseif ($request->has('datetimepickerdebut') && !$request->has('datetimepickerfin')) {
                
                $historiques =  Historique::where([
                    ['debut', '>=', $debut], 
                    ['appareil_id', '=', $id],
                    ])
                ->get();

            }elseif (!$request->has('datetimepickerdebut') && $request->has('datetimepickerfin')) {
                
                $historiques =  Historique::where([
                    ['fin', '<', $fin], 
                    ['appareil_id', '=', $id],
                    ])
                ->get();

            }else{

                $historiques =  Historique::where([
                    ['appareil_id', '=', $id],
                    ])
                ->get();         
            }

            $historiquesList = array();

            $h = array();

            foreach ($historiques as $historique) {

                $employe = Employe::find($historique->employe_id);

                $h["historique_id"] = $historique->id;
                $h["employe_id"] = $historique->employe_id;
                $h["name"] = $employe->nom . " " . $employe->prenom ;
                $h["debut"] = $historique->debut;
                $h["fin"] = $historique->fin;

                array_push($historiquesList, $h);
                unset($h);
            }
            
            return response()->json(['result' => $historiquesList], Response::HTTP_OK);
        }


        /**
         * Display the specified resource.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function appareilExistApi(Request $request)
        {
            $appareil = Appareil::find($request->input('appareil_id'));

            if ($appareil != null) {
                return response()->json(Response::HTTP_OK)->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }else
                return response()->json(Response::HTTP_NO_CONTENT)->setStatusCode(Response::HTTP_NO_CONTENT, Response::$statusTexts[Response::HTTP_NO_CONTENT]);
            
        }

    }
