<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Historique;
use DB;

class HistoriqueController extends Controller
{

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Historique $historique)
    {
        $historique->delete();
        return redirect('/appareils');
    }


     /************************************ Api methods ************************************/




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexApi()
    {
        $historiques = Historique::all();

        return response()->json($historiques, Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeApi(Request $request)
    {
        $historique = Historique::create($request->all());

        DB::table('appareils')
            ->where('id', $request->input('appareil_id'))
            ->update(['disponible' => 0]);

        return response()->json(Response::HTTP_OK);
    }


    /**
     * Display the specified resource.
     *
     * @param  App\Historique $historique
     * @return \Illuminate\Http\Response
     */
    public function showApi(Historique $historique)
    {

        return response()->json($historique, Response::HTTP_OK);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Historique $historique
     * @return \Illuminate\Http\Response
     */
    public function updateApi(Request $request, Historique $historique)
    {
        $historique->update($request->all());

        return response()->json($historique, Response::HTTP_OK);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteApi(Request $request)
    {
        Historique::destroy($request->input('id'));

        return response()->json(Response::HTTP_OK);
    }

}
