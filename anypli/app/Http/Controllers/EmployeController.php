<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Employe;
use App\Historique;
use App\Appareil;
use DB;


class EmployeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employes = Employe::paginate(15);

        return view('employes.home', compact('employes'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nom' => 'required',
            'prenom' => 'required',
            'tel' => 'max : 99999999',
        ]);

        $employe = Employe::create($request->all());


        return back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $employe = Employe::find($request->input("employe_id"));

        $employe->update($request->all());

        $employes = Employe::paginate(15);

        return view('employes.home', compact('employes'));
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Employe $employe
     * @return \Illuminate\Http\Response
     */
    public function delete(Employe $employe)
    {
        $historiques = Historique::where('employe_id', '=', $employe->id)
                                        ->get();

        foreach ($historiques as $historique) 
            {
                $historique->delete();
                                            
            }

        $employe->delete();
        return back();
    }


    /**
     * Get the 'historique' of an employe
     *
     * @param  App\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function getHistorique(Employe $employe)
    {
        $historiques = Historique::where('employe_id', '=', $employe->id)->get();
        
        return view('employes.historique', compact('historiques'));
    }


    /**
     * Search for the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $nom = $request->input('nom');
        $prenom = $request->input('prenom');

        if($request->has('nom') && $request->has('prenom')){

            $employes =  Employe::where([
                        ['nom', 'LIKE', '%'.$nom.'%'], 
                        ['prenom', 'LIKE', '%'.$prenom.'%'],
                        ])
                     ->orderBy('created_at', 'desc')
                     ->paginate(15);

        }elseif ($request->has('nom') && !$request->has('prenom')) {
            
            $employes =  Employe::where([
                        ['nom', 'LIKE', '%'.$nom.'%'], 
                        ])
                     ->orderBy('created_at', 'desc')
                     ->paginate(15);

        }elseif (!$request->has('nom') && $request->has('prenom')) {
            
            $employes =  Employe::where([
                        ['prenom', 'LIKE', '%'.$prenom.'%'], 
                        ])
                     ->orderBy('created_at', 'desc')
                     ->paginate(15);
        }else
            $employes = Employe::paginate(15);//all();


        return view('employes.home', compact('employes'));
    }



    /**
     * Search for the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchHistorique(Request $request)
    {
        $debut  = $request->input('datetimepickerdebut');
        $fin    = $request->input('datetimepickerfin');
        $id     = $request->input('employe_id');


        if($request->has('datetimepickerdebut') && $request->has('datetimepickerfin')){

            $historiques =  Historique::where([
                        ['debut', '>=', $debut], 
                        ['fin', '<', $fin],
                        ['employe_id', '=', $id],
                        ])
                        ->get();


        }elseif ($request->has('datetimepickerdebut') && !$request->has('datetimepickerfin')) {
                
                $historiques =  Historique::where([
                        ['debut', '>=', $debut], 
                        ['employe_id', '=', $id],
                        ])
                     ->get();

        }elseif (!$request->has('datetimepickerdebut') && $request->has('datetimepickerfin')) {
                

                $historiques =  Historique::where([
                        ['fin', '<', date($fin)], 
                        ['employe_id', '=', $id],
                        ])
                     ->get();
        }else{

                $historiques =  Historique::where([
                        ['employe_id', '=', $id],
                        ])
                     ->get();         
        }

        return view('employes.historique', compact('historiques'));
    } 



    /************************************ Api methods ************************************/




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexApi()
    {
        $employes = Employe::all();

        return response()->json(['result' => $employes], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeApi(Request $request)
    {
        $employe = Employe::create($request->all());

        return response()->json(Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Employe $employe
     * @return \Illuminate\Http\Response
     */
    public function showApi(Employe $employe)
    {

        return response()->json($employe, Response::HTTP_OK);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateApi(Request $request)
    {
        $employe = Employe::find($request->input("id"));

        $employe->update($request->all());

        return response()->json(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteApi(Request $request)
    {

        $historiques = Historique::where('employe_id', '=', $request->input('id'))
                                        ->get();

        foreach ($historiques as $historique) 
            {
            
            DB::table('appareils')
                ->where('id', '=', $historique->appareil_id)
                ->update(['disponible' => 1]);

                $historique->delete();
                                            
            }
        Employe::destroy($request->input('id'));

        return response()->json(Response::HTTP_OK);
    }

    /**
     * Get the 'historique' of an 'employe' .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getHistoriqueApi(Request $request)
    {
        $historiques = Historique::where('employe_id', '=', $request->input('employe_id'))
                                    ->get();

        $historiquesList = array();

        foreach ($historiques as $historique) {

            $h = array();
            
            $appareil = Appareil::find($historique->appareil_id);

            $h["historique_id"] = $historique->id;
            $h["appareil_id"] = $historique->appareil_id;
            $h["marque"] = $appareil->marque;
            $h["reference"] = $appareil->reference;
            $h["categorie"] = $appareil->categorie ;
            $h["debut"] = $historique->debut;
            $h["fin"] = $historique->fin;

            array_push($historiquesList, $h);
            unset($h);
        }
        
        return response()->json(['result' => $historiquesList], Response::HTTP_OK);
    }

}
