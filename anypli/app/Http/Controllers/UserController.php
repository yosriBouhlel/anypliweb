<?php

namespace App\Http\Controllers;

use App\User;
use App\Appareil;
use App\Mail\MemberAccount;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return User
     */
    protected function create(Request $request)
    {

       $user = $request->all();

       $this->validate($request, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required',
        ]);

       User::create([
        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'password' => bcrypt($request->input('password')),
        'image' => '/users_images/user.png',
        ]);


       \Mail::to($user['email'])->send(new MemberAccount($user)); 

       return back();
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {


    if ($request->has('password')) {

        if ($request->hasFile('image')) {

            $request->file("image")->move(public_path(). "/users_images/", $user->id."png");

            $user->update([
                'name'      => $request->input('name'),
                'email'     => $request->input('email'),
                'password'  => bcrypt($request->input('password')),
                'image'     => "/users_images/".$user->id."png",
            ]);
            
        }else{

            $user->update([
                'name'      => $request->input('name'),
                'email'     => $request->input('email'),
                'password'  => bcrypt($request->input('password')),
            ]);

        }



    }else{

        if ($request->hasFile('image')) {


            $request->file('image')->move(public_path(). "/users_images/", $user->id.".png");

            $user->update([
                'name'  => $request->input('name'),
                'email' => $request->input('email'),
                'image' => "/users_images/".$user->id.".png",
                ]);
            
        }else{


        $user->update([
            'name'      => $request->input('name'),
            'email'     => $request->input('email'),
            ]);
        }


    }


    $appareils = Appareil::all();


    return redirect('/appareils')->with(compact('appareils'));
}



/************************************ Api methods ************************************/



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeApi(Request $request)
    {
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'image' => '/users_images/user.png',
            ]);

        return response()->json(Response::HTTP_OK);
    }


        /**
         * Upload a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function uploadApi(Request $request)
        {

            if( $request->hasFile("file") ) {

                $file = $request->file("file");

                $request->file("file")->move(public_path(). "/users_images/", $request->input("filename"));


                $filename = $request->input("filename");
                $id = substr($filename,0,strlen($filename)-4);
                $user = User::find($id);

                $result = $user->update([
                    'image' => "/users_images/".$request->input("filename"),
                    ]);

            }

            if ($result) {
                return response()->json(Response::HTTP_OK);
            }else {
                return response()->json(Response::HTTP_NOT_MODIFIED);
            }
            
        }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateApi(Request $request)
    {

     $user = JWTAuth::parseToken()->authenticate();

     if ($request->has('ancienpassword')) {

             if (strcmp($user->password, bcrypt($request->input('ancienpassword')))) {
         
            $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('newpassword')),
            ]);

            return response()->json(Response::HTTP_OK)->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);

     }else{

            return response()->json(Response::HTTP_NO_CONTENT)->setStatusCode(Response::HTTP_NO_CONTENT, Response::$statusTexts[Response::HTTP_NO_CONTENT]);
     }


     }else{


        $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            ]);

        return response()->json(Response::HTTP_OK)->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
     }
    
}   



    /**
     * Delete user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteApi(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if($user->delete())

            return response()->json(Response::HTTP_OK);
        else
            return response()->json(Response::HTTP_INTERNAL_SERVER_ERROR);
    }


    

    /**
     * Get user's details
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userDetailsApi(Request $request)    
    {        
        $user = JWTAuth::parseToken()->authenticate();

        $details = User::find($user->id);

        return response()->json(['details' => $details],Response::HTTP_ACCEPTED);   
    }  


}
