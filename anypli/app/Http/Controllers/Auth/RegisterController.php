<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Mail\MemberAccount;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

use Hash;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            ]);
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return User
     */
    protected function create(Request $request)
    {

       $user = $request->all();

       $this->validate($request, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required',
        ]);

       User::create([
        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'password' => bcrypt($request->input('password')),
        'image' => '/users_images/user.png',
        ]);


       \Mail::to($user['email'])->send(new MemberAccount($user)); 

       return back();
   }




   /************************************ Api methods ************************************/




    /**
     * login the user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginApi(Request $request)
    {

        $input = $request->all(); 

        if (!$token = JWTAuth::attempt($input)) 
        {            
            return response()->json(['result' => 'wrong email or password.'],Response::HTTP_UNAUTHORIZED); 
        }

        $user = auth()->user(); 

        return response()->json(['result' => $token , "user_id" => $user->id, "email" => $user->email ,  "password" => $user->password , "name" => $user->name, "image" => $user->image ],Response::HTTP_ACCEPTED);

    }




}