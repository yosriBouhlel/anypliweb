<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use App\Employe;



Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::post('/home/membre/create', 'UserController@create');
Route::post('/home/profile/update/{user}', 'UserController@update');
Route::get('/home/profile/{user}', function (User $user) {
    return view('auth.profile', compact('user'));
});



Route::get('/appareils', 'AppareilController@index');
Route::get('/appareils/details/{appareil}', 'AppareilController@show');
Route::get('/appareils/delete/{appareil}', 'AppareilController@delete');
Route::get('/appareils/search', 'AppareilController@search');
Route::get('/appareil/searchHistorique', 'AppareilController@searchHistorique');
Route::get('/appareils/historique/{appareil}', 'AppareilController@getHistorique');
Route::get('/appareils/download/{appareil}', 'AppareilController@download');




Route::get('/historiques/delete/{historique}', 'HistoriqueController@delete');


Route::get('/employes', 'EmployeController@index');
Route::post('/employes/store', 'EmployeController@store');
Route::post('/employe/update', 'EmployeController@update');
Route::get('/employes/delete/{employe}', 'EmployeController@delete');
Route::get('/employes/historique/{employe}', 'EmployeController@getHistorique');
Route::get('/employes/search', 'EmployeController@search');
Route::get('/employes/searchHistorique', 'EmployeController@searchHistorique');
Route::get('/employes/new', function () {
    return view('employes.new');
});

Route::get('/employe/profile/{employe}', function (Employe $employe) {
    return view('employes.profile', compact('employe'));
});

