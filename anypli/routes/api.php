<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});


/**
*
* Api for the employes
*
*/

Route::get('employes', 'EmployeController@indexApi');  				// show all the employes

Route::get('historique/employes', 'EmployeController@getHistoriqueApi');  	// show all the 'historiques' of an 'employe'

Route::post('employe', 'EmployeController@storeApi');				// add a new employe

Route::put('employe/update', 'EmployeController@updateApi'); 		// update an employe

Route::delete('employes/delete', 'EmployeController@deleteApi'); // delete an employe




/**
*
* Api for the 'appareils'
*
*/

Route::get('appareils', 'AppareilController@indexApi');  				// show all the appareils

Route::get('appareil/exist', 'AppareilController@appareilExistApi');  	// test if an appareil exist or not

Route::get('appareils/historiques', 'AppareilController@getHistoriqueApi');  	// show all the 'historiques' of an 'appareil'

Route::get('appareils/historiques/search', 'AppareilController@searchHistoriqueApi');  	// search all the 'historiques' of an 'appareil' in a specific period of time

Route::get('appareils/show', 'AppareilController@showApi');  		// show the details of an appareil 

Route::post('appareil', 'AppareilController@storeApi');				// add a new appareil

Route::post('appareil/upload', 'AppareilController@uploadApi');				// upload the QR code image to the server

Route::put('appareil/update', 'AppareilController@updateApi'); 			// update an appareil

Route::delete('appareils/delete', 'AppareilController@deleteApi'); // delete an appareil



/**
*
* Api for the 'historiques'
*
*/

Route::get('historiques', 'HistoriqueController@indexApi');  				// show all the 'historiques' 

Route::get('historiques/{historique}', 'HistoriqueController@showApi');  // show the details of an historique 

Route::post('historique', 'HistoriqueController@storeApi');				// add a new historique

Route::put('historiques/{historique}', 'HistoriqueController@updateApi'); 	// update an historique

Route::delete('historiques/delete', 'HistoriqueController@deleteApi'); // delete an historique



/**
*
* Api for 'users'
*
*/


Route::get('user/login', 'Auth\RegisterController@loginApi');  		// login

Route::post('user', 'UserController@storeApi');			// add a new user

Route::post('user/upload', 'UserController@uploadApi');			// upload the user's profile image

Route::delete('user/delete', 'UserController@deleteApi');			// delete user

Route::get('user/details', 'UserController@userDetailsApi');			// user's details

Route::put('user/update', 'UserController@updateApi');			// update user


