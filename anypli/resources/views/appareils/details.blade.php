@extends('layouts.app')

@section('title')

Caractéristiques

@endsection

@section('menus')

<ul class="nav navbar-nav" >

  <li class="active" ><a href="{{ url('/appareils') }}" class="">Appareils</a></li>

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
      
      Employés <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
      <li>
        <a href="{{ url('/employes') }}">liste des employés</a>
      </li>
      <li><a href="{{ url('/employes/new') }}">Nouveau employé</a></li>
    </ul>
  </li>
</ul>

@endsection

@if(!Auth::guest())

@section('content')
<div class="container col-md-8 col-md-offset-2 " style="margin-top: 30px">
  <div class="row" style="margin-bottom: 20px">

  </div>
  <table class="table table-bordered table-responsive">

    <thead>
      <tr class="success">
        <th colspan="2" class="text-center"><h3>Caractéristiques</h3></th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <th  style="width:30%">Catégorie</th>
        <td>{{$appareil->categorie}}</td>
      </tr>
      <tr>
        <th>Marque</th>
        <td>{{$appareil->marque}}</td>
      </tr>
      <tr>
        <th>Référence</th>
        <td>{{$appareil->reference}}</td>
      </tr>
      <tr>
        <th>Microprocesseur</th>
        <td>{{$appareil->microprocesseur}}</td>
      </tr>      	
      <tr>
        <th>Mémoire cache (Mo)</th>
        <td>{{$appareil->cache}}</td>
      </tr>      	
      <tr>
        <th>Ram (Go)</th>
        <td>{{$appareil->ram}}</td>
      </tr>      	
      <tr>
        <th>Disque dur (Go)</th>
        <td>{{$appareil->disque_dur}}</td>
      </tr>      	
      <tr>
        <th>OS</th>
        <td>{{$appareil->os}}</td>
      </tr>      	
      <tr>
        <th>QR code</th>
        <td><a href="{{url('/appareils/download', [$appareil->id])}}" >Télécharger</a></td>
      </tr>              	
      <tr>
        <th>date d'ajout</th>
        <td>{{$appareil->created_at->toFormattedDateString()}}</td>
      </tr>        
      <tr>
        <th>Disponible</th>
        <td>
          @if($appareil->disponible === 1)
          OUI
          @else
          NON
          @endif
        </td>
      </tr>
    </tbody>
  </table>


</div>
@endsection

@else

<script type="text/javascript">
    window.location = "{{ route('login') }}";
</script>

@endif