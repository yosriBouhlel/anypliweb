@extends('layouts.app')


@section('title')

Liste des appareils

@endsection

@section('menus')

<ul class="nav navbar-nav" >

  <li class="active" ><a href="{{ url('/appareils') }}" >Appareils</a></li>

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

      Employés <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
      <li>
        <a href="{{ url('/employes') }}">liste des employés</a>
      </li>
      <li><a href="{{ url('/employes/new') }}">Nouveau employé</a></li>
    </ul>
  </li>
</ul>

@endsection


@if(!Auth::guest())

@section('content')
<div class="container" style="margin-top: 30px">
  <div class="row" style="margin-bottom: 50px">
    <div class="col-md-8 col-md-offset-2">

     <form class="form-inline" method="GET" action="{{ url('/appareils/search') }}">

      {{csrf_field()}}

      <div class="form-group" style="margin-right: 30px">
        <label for="categorie">Catégories :</label>  
        <select name="categorie">
          <option value="ordinateurs">Ordinateurs</option>
          <option value="smartphones">Smartphones</option>
          <option value="tablettes">Tablettes</option>
        </select> 
      </div>

      <div class="form-group" style="margin-right: 30px">
        <label for="marques">Marques :</label>
        <input type="text" class="form-control" id="marques" name="marques">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-default">Filtrer</button>
      </div>      
      <div class="form-group">
      <a href="{{ url('/appareils') }}" class="btn btn-default" role="button">Annuler</a>
      </div>

    </form> 

  </div>
</div>

<table class="table table-bordered">
  <thead>
    <tr class="success">
      <th class="text-center">Id</th>
      <th class="text-center">Catégorie</th>
      <th class="text-center">Marque</th>
      <th class="text-center">Référence</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>

  <tbody>
    @foreach($appareils as $appareil)
    <tr>
      <td class="text-center">{{$appareil->id}}</td>
      <td>{{$appareil->categorie}}</td>
      <td>{{$appareil->marque}}</td>
      <td><a href="{{url('/appareils/details', [$appareil->id])}}" >{{$appareil->reference}}</a></td>
      <td class="text-center">

        <a href="{{url('/appareils/download', [$appareil->id])}}" style="margin-right: 20px">QR code</a>
        <a href="{{url('/appareils/historique', [$appareil->id])}}" style="margin-right: 20px">Historique</a>
        <a href="{{url('/appareils/delete', [$appareil->id])}}" onclick="return confirm('Voulez-vous vraiment supprimez cette appareil ?');">Supprimer</a>

      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
{{ $appareils->links('vendor.pagination.custom') }}
</div>
@endsection

@else

<script type="text/javascript">
    window.location = "{{ route('login') }}";
</script>

@endif


