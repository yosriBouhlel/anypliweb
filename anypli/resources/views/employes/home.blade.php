@extends('layouts.app')

@section('title')

Liste des employés

@endsection

@section('menus')

<ul class="nav navbar-nav">

  <li  ><a href="{{ url('/appareils') }}" >Appareils</a></li>

  <li class="dropdown active">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
      
      Employés <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
      <li>
        <a href="{{ url('/employes') }}">liste des employés</a>
      </li>
      <li><a href="{{ url('/employes/new') }}">Nouveau employé</a></li>
    </ul>
  </li>
</ul>

@endsection

@if(!Auth::guest())

@section('content')
<div class="container" style="margin-top: 30px">
  <div class="row" style="margin-bottom: 50px">
    <div class="col-md-8 col-md-offset-2">

     <form class="form-inline" method="GET" action="{{ url('/employes/search') }}">

      {{csrf_field()}}

      <div class="form-group" style="margin-right: 30px">
        <label for="nom">Nom :</label>  
        <input type="text" class="form-control" id="nom" name="nom">
      </div>

      <div class="form-group" style="margin-right: 10px">
        <label for="prenom">Prénom :</label>
        <input type="text" class="form-control" id="prenom" name="prenom">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-default">Filtrer</button>
      </div>

      <div class="form-group">
      <a href="{{ url('/employes') }}" class="btn btn-default" role="button">Annuler</a>
      </div>

    </form> 

  </div>
</div>

<table class="table table-bordered">
  <thead>
    <tr class="success">
      <th class="text-center">Id</th>
      <th class="text-center">Nom & Prénom</th>
      <th class="text-center">Spécialité</th>
      <th class="text-center">Email</th>
      <th class="text-center">Tél</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>

  <tbody>
    @foreach($employes as $employe)
    <tr>
      <td class="text-center">{{$employe->id}}</td>
      <td><a href="{{url('/employe/profile', [$employe])}}" >{{$employe->nom}} {{$employe->prenom}}</a></td>
      <td>{{$employe->specialite}}</td>
      <td>{{$employe->email}}</td>
      <td>{{$employe->tel}}</td>
      <td class="text-center">

        <a href="{{url('/employes/historique', [$employe->id])}}" style="margin-right: 20px" >Historique</a>
        <a href="{{url('/employes/delete', [$employe->id])}}" onclick="return confirm('Voulez-vous vraiment supprimez ce employé ?');">Supprimer</a>

      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
{{ $employes->links('vendor.pagination.custom') }}
</div>
@endsection


@else

<script type="text/javascript">
    window.location = "{{ route('login') }}";
</script>

@endif