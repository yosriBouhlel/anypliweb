@extends('layouts.app')

@section('title')

Nouveau employé

@endsection

@section('menus')

<ul class="nav navbar-nav">
  <li  ><a href="{{ url('/appareils') }}" class="">Appareils</a></li>
  <li class="dropdown active">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
      
      Employés <span class="caret"></span>
  </a>

  <ul class="dropdown-menu" role="menu">
      <li>
        <a href="{{ url('/employes') }}">liste des employés</a>
    </li>
    <li><a href="{{ url('/employes/new') }}">Nouveau employé</a></li>
</ul>
</li>
</ul>

@endsection

@if(!Auth::guest())

@section('content')
<div class="container" style="margin-top: 90px">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Nouveau employé</div>
                <div class="panel-body">
                   <form class="form-horizontal" method="POST" action="{{ url('/employes/store') }}">

                    {{csrf_field()}}

                    <div class="form-group{{ $errors->has('nom') ? ' has-error' : '' }}">
                        <label for="nom" class="col-md-4 control-label">Nom :</label>

                        <div class="col-md-6">
                            <input id="nom" type="text" class="form-control" name="nom"  required autofocus>

                            @if ($errors->has('nom'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nom') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>                



                    <div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }}">
                        <label for="prenom" class="col-md-4 control-label">Prénom :</label>

                        <div class="col-md-6">
                            <input id="prenom" type="text" class="form-control" name="prenom" required autofocus>

                            @if ($errors->has('prenom'))
                            <span class="help-block">
                                <strong>{{ $errors->first('prenom') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>                



                    <div class="form-group{{ $errors->has('specialite') ? ' has-error' : '' }}">
                        <label for="specialite" class="col-md-4 control-label">Spécialité :</label>

                        <div class="col-md-6">
                            <input id="specialite" type="text" class="form-control" name="specialite" required autofocus>

                            @if ($errors->has('specialite'))
                            <span class="help-block">
                                <strong>{{ $errors->first('specialite') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>                


                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Email :</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email"  required autofocus>

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>                


                    <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
                        <label for="tel" class="col-md-4 control-label">Tél :</label>

                        <div class="col-md-6">
                            <input id="tel" type="text" class="form-control" name="tel"  required autofocus>

                            @if ($errors->has('tel'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tel') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Ajouter
                            </button>
                        </div>
                    </div>
                    
                    @if ($errors->any())
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                    @endif

                </form> 
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@else

<script type="text/javascript">
    window.location = "{{ route('login') }}";
</script>

@endif