@extends('layouts.app')

@section('title')

Historique de l'employé

@endsection

@section('menus')

<ul class="nav navbar-nav">
  <li  ><a href="{{ url('/appareils') }}" class="">Appareils</a></li>
  <li class="dropdown active">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
      
      Employés <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
      <li>
        <a href="{{ url('/employes') }}">liste des employés</a>
      </li>
      <li><a href="{{ url('/employes/new') }}">Nouveau employé</a></li>
    </ul>
  </li>
</ul>

@endsection

@if(!Auth::guest())

@section('content')
<div class="container" style="margin-top: 30px">
  <div class="row" style="margin-bottom: 50px">
    <div class="col-md-8 col-md-offset-2">

      <form class="form-inline" method="GET" action="{{ url('/employes/searchHistorique') }}">

        {{csrf_field()}}

        <div class="form-group" style="margin-right: 30px">
          <label for="debut">Début :</label>
          <div class='input-group date'>
            <input type="text" id="datetimepickerdebut" name="datetimepickerdebut">&nbsp;
          </div>
        </div>
        
        
        <div class="form-group" style="margin-right: 30px">
          <label for="fin">Fin :</label>
          <div class='input-group date'>
            <input type="text" id="datetimepickerfin" name="datetimepickerfin">&nbsp;
          </div>
        </div>

        <input type="hidden" name="employe_id" value="<?php if(!$historiques->isEmpty()) echo $historiques{0}->employe_id; ?>" >

        <div class="form-group">
          <button type="submit" class="btn btn-default">Filtrer</button>
        </div>              

     <!-- <div class="form-group">
      <a href="{{ url('/employes/historique', [<?php if(!$historiques->isEmpty()) echo $historiques{0}->employe_id; ?>]) }}" class="btn btn-default" role="button">Annuler</a>
      </div> -->

      </form>  

    </div>
  </div>

  <table class="table table-bordered">
    <thead>
      <tr class="success">
        <th class="text-center">Catégorie</th>
        <th class="text-center">Marque</th>
        <th class="text-center">Référence</th>
        <th class="text-center">Date début</th>
        <th class="text-center">Date fin</th>
        <th class="text-center">Actions</th>
      </tr>
    </thead>

    <tbody>

      @if(!$historiques->isEmpty())

      @foreach($historiques as $historique)
      <tr>
        <?php 
        $appareil = App\Appareil::find($historique->appareil_id);
        ?>
        <td class="text-center">{{$appareil->categorie}}</td>
        <td>{{$appareil->marque}}</td>
        <td><a href="{{url('/appareils/details', [$appareil->id])}}" >{{$appareil->reference}}</a></td>
        <td>{{$historique->debut}}</td>

        @if($historique->fin == null)
        <td>n'est pas spécifié</td>
        @else
        <td>{{$historique->fin}}</td>
        @endif
        
        <td class="text-center"><a href="{{url('/historiques/delete', [$historique->id])}}" onclick="return confirm('Voulez-vous vraiment supprimez ce historique ?');">Supprimer</a></td>
      </tr>
      @endforeach

      @endif
    </tbody>
  </table>
</div>

</div>
@endsection




@section('javaScript')



<script>
  $( function() {

    $( "#datetimepickerdebut" ).datepicker({
      altField: "#datetimepickerdebut",
      altFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    }).on("dp.change", function (e) {
      $('#datetimepickerfin').data("DateTimePicker").minDate(e.date);
    });   


    $( "#datetimepickerfin" ).datepicker({
      altField: "#datetimepickerfin",
      altFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    }).on("dp.change", function (e) {
      $('#datetimepickerdebut').data("DateTimePicker").maxDate(e.date);
    });
  });
</script>
@endsection

@else

<script type="text/javascript">
    window.location = "{{ route('login') }}";
</script>

@endif