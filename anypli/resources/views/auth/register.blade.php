@extends('layouts.app')

@section('title')

Ajouter membre

@endsection

@section('menus')

<ul class="nav navbar-nav" >

  <li><a href="{{ url('/appareils') }}" class="">Appareils</a></li>

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

      Employés <span class="caret"></span>
  </a>

  <ul class="dropdown-menu" role="menu">
      <li>
        <a href="{{ url('/employes') }}">liste des employés</a>
    </li>
    <li><a href="{{ url('/employes/new') }}">Nouveau employé</a></li>
</ul>
</li>
</ul>

@endsection

@if(!Auth::guest())

@section('content')
<div class="container" style="margin-top: 90px">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ajouter membre</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('/home/membre/create') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nom et prénom</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">

                                <input id="password" type="hidden" class="form-control" name="password" value="<?php echo str_random(10); ?>"  required>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Ajouter
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
@endsection

@else

<script type="text/javascript">
    window.location = "{{ route('login') }}";
</script>

@endif